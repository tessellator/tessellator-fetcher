# Tessellator::Fetcher

Document fetcher for Tessellator.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tessellator-fetcher'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tessellator-fetcher

## Usage

```ruby
require 'tessellator/fetcher'

fetcher = Tessellator::Fetcher.new do |fetcher|
  fetcher.version = Tessellator::Fetcher::VERSION
  fetcher.user_agent = "TessellatorFetcher/v#{fetcher.version}"
  fetcher.http_redirect_limit = 10
  fetcher.pages_path = '/path/to/internal/pages'
end

fetcher.call('get', 'https://duckie.co')
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on Gitlab at https://gitlab.com/tessellator/tessellator-fetcher.
