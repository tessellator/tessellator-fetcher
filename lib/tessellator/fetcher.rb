require 'tessellator/fetcher/version'
#require 'tessellator/debug'
require 'tessellator/fetcher/helpers'
require 'tessellator/fetcher/config'
require 'cacert'

Cacert.set_in_env

if !defined?(Tessellator::Debug)
  module Tessellator
    module Debug;
      def debug(*_args); end
      def debug_print_call(*_args); end
    end
  end
end

class Tessellator::Fetcher
  attr_accessor :config

  %w[data file http internal].each do |type|
    require "tessellator/fetcher/request/#{type}"
  end

  NotSetError = Class.new(StandardError)

  include Tessellator::Debug

  def initialize(hash={}, &block)
    @config = Config.new(hash, &block)
  end

  def call(method, url, parameters={})
    debug_print_call

    uri = Helpers.safe_uri(url)

    Request.handler_for_scheme(uri.scheme).call(@config, method, url, parameters)
  end
end
