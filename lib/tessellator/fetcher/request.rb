require 'tessellator/fetcher/version'
require 'uri'
require 'openssl/better_defaults'
require 'net/https'

class Tessellator::Fetcher
  require 'tessellator/fetcher/response'

  class Request
    def self.handler_for_scheme(scheme)
      requester = self.constants.map(&method(:const_get)).find {|x| x.schemes.include?(scheme)}

      unless requester
        raise NotImplementedError, "No way to handle #{uri.scheme} URIs."
      end

      requester
    end
  end

  class BaseRequest
    attr_accessor :config

    def self.schemes
      const_get(:SCHEMES)
    end

    def self.call(config, method, url, parameters, options={})
      self.new(config).call(method, url, parameters, options)
    end

    def initialize(config)
      @config = config
    end

    def call(method, url, parameters, options={})

      uri = Helpers.safe_uri(options[:url_override] || url)

      unless self.class.schemes.include?(uri.scheme)
        raise NotImplementedError, "#{self.name} cannot handle #{uri.scheme} URIs."
      end

      Response.new(*fetch(method, url, parameters, options))
    end

    private
    def fetch(method, url, parameters, options)
      raise NotImplementedError
    end
  end
end
