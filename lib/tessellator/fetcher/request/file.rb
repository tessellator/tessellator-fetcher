require 'tessellator/fetcher/version'
require 'uri'
require 'base64'

class Tessellator::Fetcher
  require 'tessellator/fetcher/request'

  class Request::File < BaseRequest
    SCHEMES = %w[file]

    private
    def fetch(method, url, parameters, options)
      if url =~ /^([^:]+):([^;,]+)(;charset=[^;,]+)?(;base64)?,(.*)$/
        scheme, mime_type, charset, base64, data = $1, $2, $3, $4, $5

        charset = charset.split('=').last if charset
      end

      unless scheme == 'data'
        raise ArgumentError, "expected data URI, got #{scheme} URI"
      end

      data = URI.decode(data)
      data = Base64.decode64(data) if base64

      headers = {
        'content-type' => "#{mime_type}"
      }

      headers['content-type'] += "; charset=#{charset}" if charset

      [data, headers, url]
    end
  end
end
