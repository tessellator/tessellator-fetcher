require 'tessellator/fetcher/helpers'
require 'uri'
require 'openssl/better_defaults'
require 'net/https'
require 'heresy/string'
require 'mayhaps'

class Tessellator::Fetcher
  require 'tessellator/fetcher/request'

  class Request::HTTP < BaseRequest
    SCHEMES     = %w[https http]

    MethodError = Class.new(ArgumentError)
    RedirectLimitError = Class.new(StandardError)

    HTTP_METHODS = %w[
      get head post put delete options trace patch
    ]

    def default_headers
      {
        'User-Agent' => config.user_agent,
      }
    end

    private
    def method_constant(method)
      method = method.to_s.downcase

      raise MethodError, "invalid HTTP method: #{method}" unless HTTP_METHODS.include?(method)

      Net::HTTP.const_get(method.capitalize)
    end

    def fetch(method, url, parameters, options, limit = config.http_redirect_limit)
      # TODO: Actually handle this exception.
      if limit <= 0
        raise RedirectLimitError, "request exceeded redirect limit (#{Tessellator::Fetcher.http_redirect_limit})."
      end

      uri = Tessellator::Fetcher::Helpers.safe_uri(url)

      options[:use_ssl] = (uri.scheme == 'https')

      Net::HTTP.start(uri.host, uri.port, options) do |http|
        request = Net::HTTP::Get.new uri

        default_headers.each do |header, value|
          request[header] = value
        end

        (options[:headers] || []).each do |header, value|
          request[header] = value
        end

        response = http.request(request)

        if response.is_a?(Net::HTTPRedirection)
          if Tessellator.respond_to?(:debug) && Tessellator.debug
            $stderr.puts "[DEBUG] #{uri} redirects to #{response['location']}."
          end

          if response['location'].start_with?('/')
            location = uri.dup
            location.path = response['location']

            # TODO: Figure out of querystrings are supposed to be passed along
            #       with redirects. If not, clear them here!
          else
            location = response['location']
          end

          return fetch(method, location.to_s, parameters, options, limit - 1)
        end

        [response.body, +response.maybe.to_hash, uri.to_s]
      end
    end
  end
end
