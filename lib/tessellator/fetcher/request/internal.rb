require 'heresy/string'
require 'uri'

class Tessellator::Fetcher
  require 'tessellator/fetcher/request'

  class Request::Internal < BaseRequest
    SCHEMES = %w[about]

    def fetch(method, url, parameters, options = {}, depth = 0)
      uri  = Helpers.safe_uri(url)
      body = fetch_file(uri)

      data = {
        url:          options[:url_override] || url,
        version:      @config.version,
        user_agent:   @config.user_agent,
      }

      if body
        [body.format(data), {'content-type' => 'text/html'}, data[:url]]
      elsif depth > 0
        error_text =
          <<~EOF
            INTERNAL FETCHER ERROR: Repeated failed calls to #{self.class}.new.fetch

            Actual url = #{url.inspect}
            url_override = #{options[:url_override].inspect}
          EOF
        [
          error_text,
          {'content-type' => 'text/plain'},
          data[:url]
        ]
      else
        fetch('GET', 'errors:404', {}, {url_override: url}, depth + 1)
      end
    end

    def fetch_file(uri)
      filename = uri.opaque + '.html'

      return nil if filename.start_with?('.') || filename.start_with?('/')

      file_path = File.join(@config.pages_path, uri.scheme, filename)

      return nil unless File.file?(file_path)

      open(file_path).read
    end
  end
end
