require 'spec_helper'
require 'base64'

RSpec.describe Tessellator::Fetcher do
  subject {
    Tessellator::Fetcher.new do |config|
      config.pages_path = File.expand_path("../data/", __dir__)
    end
  }

  # TODO: HTTP URIs
  #it 'can fetch HTTP URIs' do
  # #...
  #end

  describe 'Internal URIs' do
    it 'can fetch an internal URI' do
      doc = subject.call('get', 'about:test')
      expect(doc.body).to eq "hi\n"
    end
  end

  # Data URIs
  describe 'Data URIs' do
    doc_source = '<p>Hello, world!</p>'
    base64_doc_source = Base64.encode64(doc_source)

    it 'can fetch plaintext Data URIs with a character set' do
      doc = subject.call('[unused]', "data:text/html;charset=utf-8,#{doc_source}")
      expect(doc.headers['content-type'].split(';').map(&:strip)).to eq(['text/html', 'charset=utf-8'])
      expect(doc.body).to eq(doc_source)
    end

    it 'can fetch plaintext Data URIs without a character set' do
      doc = subject.call('[unused]', "data:text/html,#{doc_source}")
      expect(doc.headers['content-type']).to eq('text/html')
      expect(doc.body).to eq(doc_source)
    end

    it 'can fetch base64-encoded Data URIs with a character set' do
      doc = subject.call('[unused]', "data:text/html;charset=utf-8;base64,#{base64_doc_source}")
      expect(doc.headers['content-type'].split(';').map(&:strip)).to eq(['text/html', 'charset=utf-8'])
      expect(doc.body).to eq(doc_source)
    end

    it 'can fetch base64-encoded Data URIs without a character set' do
      doc = subject.call('[unused]', "data:text/html;base64,#{base64_doc_source}")
      expect(doc.headers['content-type']).to eq('text/html')
      expect(doc.body).to eq(doc_source)
    end
  end
end
